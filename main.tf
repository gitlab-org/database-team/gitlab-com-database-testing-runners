resource "google_compute_instance" "proxy_instance" {
  name         = "proxy"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-minimal-2404-noble-amd64-v20250203"
    }
  }

  network_interface {
    network = "default"
  }

  tags = [
    "deny-all-egress",
    "allow-dns-egress",
    "allow-web-egress"
  ]

  metadata_startup_script = <<-SCRIPT
    set -e

    sudo apt-get update
    sudo apt-get install -y squid
    echo "http_port ${var.proxy_port}" | sudo tee /etc/squid/squid.conf
    echo "acl gitlab dstdomain .gitlab.com" | sudo tee -a /etc/squid/squid.conf
    echo "acl gitlab dstdomain ops.gitlab.net" | sudo tee -a /etc/squid/squid.conf
    echo "acl gitlaborggitlabio dstdomain gitlab-org.gitlab.io" | sudo tee -a /etc/squid/squid.conf
    echo "acl objectstorage dstdomain storage.googleapis.com" | sudo tee -a /etc/squid/squid.conf
    echo "acl cloudfront dstdomain .cloudfront.net" | sudo tee -a /etc/squid/squid.conf
    echo "acl ubuntu dstdomain .ubuntu.com" | sudo tee -a /etc/squid/squid.conf
    echo "http_access allow gitlab" | sudo tee -a /etc/squid/squid.conf
    echo "http_access allow gitlaborggitlabio" | sudo tee -a /etc/squid/squid.conf
    echo "http_access allow objectstorage" | sudo tee -a /etc/squid/squid.conf
    echo "http_access allow cloudfront" | sudo tee -a /etc/squid/squid.conf
    echo "http_access allow ubuntu" | sudo tee -a /etc/squid/squid.conf
    echo "http_access deny all" | sudo tee -a /etc/squid/squid.conf
    echo "forwarded_for delete" | sudo tee -a /etc/squid/squid.conf
    sudo systemctl restart squid
  SCRIPT
}

resource "gitlab_user_runner" "database_worker_runner" {
  runner_type  = "project_type"
  project_id   = var.gitlab_project_id
  access_level = "ref_protected"
  description  = "Database runner used for running gitlab-housekeeper with access to thin clones"
  locked       = true
  untagged     = false

  tag_list = [
    "gitlab-housekeeper"
  ]
}

resource "google_compute_instance" "database_worker_instance" {
  name                      = "database-worker"
  machine_type              = "e2-standard-2"
  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2404-noble-amd64-v20250130"
      size  = 100
    }
  }

  network_interface {
    network = "default"
  }

  tags = [
    "deny-all-egress",
    "allow-proxy",
    "allow-ssh-egress"
  ]

  metadata_startup_script = <<-SCRIPT
    set -e

    timeout 300 bash -c 'until curl -s -f -o /dev/null -w %%{http_code} "http://${google_compute_instance.proxy_instance.network_interface.0.network_ip}:${var.proxy_port}" | grep -q 400; do sleep 5; done'
    echo "export http_proxy=http://${google_compute_instance.proxy_instance.network_interface.0.network_ip}:${var.proxy_port}" | sudo tee /etc/profile.d/99_http_proxy.sh
    echo "export https_proxy=http://${google_compute_instance.proxy_instance.network_interface.0.network_ip}:${var.proxy_port}" | sudo tee -a /etc/profile.d/99_http_proxy.sh
    chmod a+x /etc/profile.d/99_http_proxy.sh
    source /etc/profile.d/99_http_proxy.sh
    echo 'Acquire::http::Proxy "http://${google_compute_instance.proxy_instance.network_interface.0.network_ip}:${var.proxy_port}";' | sudo tee /etc/apt/apt.conf.d/99http-proxy
    echo 'Acquire::https::Proxy "http://${google_compute_instance.proxy_instance.network_interface.0.network_ip}:${var.proxy_port}";' | sudo tee -a /etc/apt/apt.conf.d/99http-proxy
    sudo mkdir -p /etc/systemd/system/docker.service.d
    echo '[Service]' | sudo tee /etc/systemd/system/docker.service.d/http-proxy.conf
    echo 'Environment="HTTP_PROXY=http://${google_compute_instance.proxy_instance.network_interface.0.network_ip}:${var.proxy_port}"' | sudo tee -a /etc/systemd/system/docker.service.d/http-proxy.conf
    echo 'Environment="HTTPS_PROXY=http://${google_compute_instance.proxy_instance.network_interface.0.network_ip}:${var.proxy_port}"' | sudo tee -a /etc/systemd/system/docker.service.d/http-proxy.conf
    sudo mkdir -p /etc/systemd/system/gitlab-runner.service.d
    sudo cp /etc/systemd/system/docker.service.d/http-proxy.conf /etc/systemd/system/gitlab-runner.service.d/http-proxy.conf
    sudo apt-get update
    curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo --preserve-env=http_proxy,https_proxy bash
    sudo apt-get install -y docker.io gitlab-runner
    sudo --preserve-env=http_proxy,https_proxy gitlab-runner register \
      --non-interactive \
      --url "https://ops.gitlab.net/" \
      --token "${gitlab_user_runner.database_worker_runner.token}" \
      --executor "docker" \
      --docker-disable-cache \
      --docker-image alpine:latest \
      --description "database-worker" \
      --env http_proxy=$http_proxy \
      --env https_proxy=$https_proxy
    echo "0 11 * * * /usr/share/gitlab-runner/clear-docker-cache prune-volumes; docker image prune -a -f --filter until=24h" | sudo crontab -u root -
  SCRIPT
}
