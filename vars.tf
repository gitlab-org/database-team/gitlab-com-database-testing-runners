variable "gitlab_api_v4_url" {
  type = string
}

variable "gitlab_token" {
  type = string
}

variable "gitlab_project_id" {
  type = number
}

variable "gcp_project" {
  type    = string
  default = "group-database-239135"
}

variable "proxy_port" {
  type    = string
  default = "3128"
}
