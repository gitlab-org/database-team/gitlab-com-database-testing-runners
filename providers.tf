provider "google" {
  project = var.gcp_project
  region  = "us-central1"
  zone    = "us-central1-a"
}

provider "gitlab" {
  base_url = var.gitlab_api_v4_url
  token    = var.gitlab_token
}
