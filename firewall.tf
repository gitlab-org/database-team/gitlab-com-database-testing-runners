resource "google_compute_firewall" "isolated_instance_allow_egress" {
  destination_ranges = ["0.0.0.0/0"]
  direction          = "EGRESS"
  name               = "isolated-instance-allow-egress"
  network            = google_compute_network.default.id
  priority           = 100
  target_tags        = ["isolated-instance"]
  allow {
    ports    = ["53"]
    protocol = "udp"
  }
}

resource "google_compute_firewall" "isolated_instance_deny_all_egress" {
  destination_ranges = ["0.0.0.0/0"]
  direction          = "EGRESS"
  name               = "isolated-instance-deny-all-egress"
  network            = google_compute_network.default.id
  priority           = 1000
  target_tags        = ["isolated-instance"]
  deny {
    ports    = []
    protocol = "all"
  }
}

resource "google_compute_firewall" "isolated_instance_allow_ssh" {
  description        = "SSH to dblab"
  destination_ranges = ["34.168.223.177/32", "34.138.148.39/32"]
  direction          = "EGRESS"
  name               = "isolated-instance-allow-ssh"
  network            = google_compute_network.default.id
  priority           = 100
  target_tags        = ["isolated-instance"]
  allow {
    ports    = ["22"]
    protocol = "tcp"
  }
}

resource "google_compute_firewall" "isolated_instance_allow_egress_gitlab" {
  description        = "ops.gitlab.net"
  destination_ranges = ["172.65.19.90/32"]
  direction          = "EGRESS"
  name               = "isolated-instance-allow-egress-gitlab"
  network            = google_compute_network.default.id
  priority           = 498
  target_tags        = ["isolated-instance"]
  allow {
    ports    = ["443"]
    protocol = "tcp"
  }
}

resource "google_compute_firewall" "deny_all_egress" {
  destination_ranges = ["0.0.0.0/0"]
  direction          = "EGRESS"
  name               = "deny-all-egress"
  network            = google_compute_network.default.id
  priority           = 1000
  target_tags        = ["deny-all-egress"]
  deny {
    ports    = []
    protocol = "all"
  }
}

resource "google_compute_firewall" "allow_dns_egress_tcp" {
  destination_ranges = ["0.0.0.0/0"]
  direction          = "EGRESS"
  name               = "allow-dns-egress-tcp"
  network            = google_compute_network.default.id
  priority           = 500
  target_tags        = ["allow-dns-egress"]
  allow {
    ports    = ["53"]
    protocol = "tcp"
  }
}

resource "google_compute_firewall" "allow_dns_egress_udp" {
  destination_ranges = ["0.0.0.0/0"]
  direction          = "EGRESS"
  name               = "allow-dns-egress-udp"
  network            = google_compute_network.default.id
  priority           = 500
  target_tags        = ["allow-dns-egress"]
  allow {
    ports    = ["53"]
    protocol = "udp"
  }
}

resource "google_compute_firewall" "allow_web_egress" {
  destination_ranges = ["0.0.0.0/0"]
  direction          = "EGRESS"
  name               = "allow-web-egress"
  network            = google_compute_network.default.id
  priority           = 500
  target_tags        = ["allow-web-egress"]
  allow {
    ports    = ["80", "443"]
    protocol = "tcp"
  }
}

resource "google_compute_firewall" "allow_ssh_egress" {
  description        = "SSH to dblab"
  destination_ranges = ["34.168.223.177/32", "34.138.148.39/32"]
  direction          = "EGRESS"
  name               = "allow-ssh-egress"
  network            = google_compute_network.default.id
  priority           = 500
  target_tags        = ["allow-ssh-egress"]
  allow {
    ports    = ["22"]
    protocol = "tcp"
  }
}

resource "google_compute_firewall" "allow_proxy" {
  destination_ranges = ["${google_compute_instance.proxy_instance.network_interface.0.network_ip}/32"]
  direction          = "EGRESS"
  name               = "allow-proxy"
  network            = google_compute_network.default.id
  priority           = 500
  target_tags        = ["allow-proxy"]
  allow {
    ports    = [var.proxy_port]
    protocol = "tcp"
  }
}
